// Fix the 2 issues in the fetch
async function getRandomString(number, length, format) {
    let response = 
    await fetch("https://www.random.org/strings/?num="+number+"&digits="+length+"&upperalpha=on&loweralpha=on&unique=on&format="+format+"&rnd=new");
    return await response.text();
}

// Call getRandomString() 10 times with increasing by 1 (length++)
getRandomString(2, 16, "plain").then(data => console.log(data));
